from environs import Env
import os

env = Env()
env.read_env()
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

ENV = env.str("FLASK_ENV", default="production")
DEBUG = ENV == "development"
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR,
                                                      '../tests/app.db')
SECRET_KEY = 'sfdijpeok-prkf34646XQWE&*'
DEBUG_TB_ENABLED = DEBUG
DEBUG_TB_INTERCEPT_REDIRECTS = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
API_PREFIX = '/api/v1'
SQLALCHEMY_ECHO = False
