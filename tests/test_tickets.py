from flask_rest_app.tickets.models import Comment, Ticket

from flask_rest_app.settings import API_PREFIX


def creating_tickets(cnt, client, db):
    url = f'{API_PREFIX}/tickets'
    for number in range(cnt):
        data = {
            'theme': f'test_create_ticket_#{number}',
            'text': 'some text for testing here',
            'email': 'fh@iuier.com',
        }
        client.post(url, json=data)


def creating_comments(cnt, client, db):
    url = f'{API_PREFIX}/comments'
    tickets_num = 1
    creating_tickets(tickets_num, client, db)
    ticket = Ticket.query.filter_by(id=1).first()
    for number in range(cnt):
        comment = {
            'ticket_id': ticket.id,
            'text': f'Comment_#{number}. some text here',
            'email': '123@example.com'
        }
        client.post(url, json=comment)


def ticket_change_status(db, status='closed'):
    ticket = Ticket.query.first()
    ticket.status = status
    db.session.commit()


def test_ticket_create(client, db):
    data = {
        'theme': 'test_create_ticket',
        'text': 'some text for testing here',
        'email': 'fh@iuier.com',
        'status': 'closed'
    }
    url = f'{API_PREFIX}/tickets'
    response = client.post(url, json=data)
    assert response.content_type == 'application/json'
    assert response.status_code == 201
    assert response.get_json()['status'] == 'open'


def test_get_ticket_list(client, db):
    tickets_cnt = 10
    url = f'{API_PREFIX}/tickets'
    creating_tickets(tickets_cnt, client, db)
    response = client.get(url)
    json_response = response.get_json()
    assert response.status_code == 200
    assert json_response['count'] == len(Ticket.query.all())
    assert json_response['next'] == f'{url}?start=3&limit=2'


def test_get_tickets_list_with_limits(client, db):
    tickets_cnt = 15
    creating_tickets(tickets_cnt, client, db)
    base_url = f'{API_PREFIX}/tickets'
    response = client.get(f'{base_url}?limit=10')
    json_response = response.get_json()
    assert response.status_code == 200
    assert json_response['next'] == f'{base_url}?start=11&limit=10'


def test_get_one_ticket(client, db):
    tickets_cnt = 10
    creating_tickets(tickets_cnt, client, db)
    pk = 10
    url = f'{API_PREFIX}/ticket/{pk}'
    response = client.get(url)
    json_response = response.get_json()
    assert response.status_code == 200
    assert json_response['id'] == pk


def test_get_not_found_ticket(client, db):
    pk = 100
    url = f'{API_PREFIX}/ticket/{pk}'
    response = client.get(url)
    assert response.status_code == 404


def test_add_comment(client, db):
    tickets_cnt = 1
    creating_tickets(tickets_cnt, client, db)
    ticket = Ticket.query.filter(Ticket.status.isnot('closed')).first()
    comment = {
        'ticket_id': ticket.id,
        'text': 'some text here',
        'email': '123@example.com'
    }
    url = f'{API_PREFIX}/comments'
    response = client.post(url, json=comment)
    assert response.status_code == 201


def test_add_comment_to_closed_ticket(client, db):
    tickets_cnt = 1
    creating_tickets(tickets_cnt, client, db)
    ticket_change_status(db)
    ticket = Ticket.query.filter_by(status='closed').first()
    comment = {
        'ticket_id': ticket.id,
        'text': 'some text here',
        'email': '123@example.com'
    }
    url = f'{API_PREFIX}/comments'
    response = client.post(url, json=comment)
    assert response.status_code == 400
    assert response.get_json()['error'] == "Ticket closed. " \
                                           "You can't add a comment"


def test_get_ticket_comments(client, db):
    url = f'{API_PREFIX}/ticket/1/comments'
    comments_cnt = 3
    creating_comments(comments_cnt, client, db)
    response = client.get(url)
    json_response = response.get_json()
    assert response.status_code == 200
    assert json_response['count'] == len(Comment.query.all())


def test_change_status_for_closed_ticket(client, db):
    tickets_cnt = 1
    creating_tickets(tickets_cnt, client, db)
    ticket_change_status(db)
    closed_ticket = Ticket.query.filter_by(status='closed').first()
    closed_ticket = {x.name: getattr(closed_ticket, x.name) for x in
                     closed_ticket.__table__.columns}
    closed_ticket['status'] = 'wip'
    url = f"{API_PREFIX}/ticket/{closed_ticket['id']}"
    response = client.put(url, json=closed_ticket)
    assert response.status_code == 400
    assert response.get_json()['error'] == "Ticket closed. You can't modify it"


def test_change_status_for_open_ticket(client, db):
    tickets_cnt = 1
    creating_tickets(tickets_cnt, client, db)
    new_status = 'wip'
    ticket = Ticket.query.filter_by(status='open').first()
    ticket = {x.name: getattr(ticket, x.name) for x in ticket.__table__.columns}
    ticket['status'] = new_status

    url = f"{API_PREFIX}/ticket/{ticket['id']}"
    response = client.put(url, json=ticket)
    assert response.status_code == 202
    assert response.get_json()['status'] == new_status


def test_change_status_for_answered_ticket_success(client, db):
    tickets_cnt = 1
    creating_tickets(tickets_cnt, client, db)
    new_status = 'wip'
    ticket_change_status(db, status='answered')
    ticket = Ticket.query.first()
    ticket = {x.name: getattr(ticket, x.name) for x in ticket.__table__.columns}
    ticket['status'] = new_status

    url = f"{API_PREFIX}/ticket/{ticket['id']}"
    response = client.put(url, json=ticket)
    assert response.status_code == 202
    assert response.get_json()['status'] == new_status


def test_not_modified_status(client, db):
    tickets_cnt = 1
    creating_tickets(tickets_cnt, client, db)
    new_status = 'open'
    ticket = Ticket.query.first()
    ticket = {x.name: getattr(ticket, x.name) for x in ticket.__table__.columns}
    ticket['status'] = new_status
    url = f"{API_PREFIX}/ticket/{ticket['id']}"
    response = client.put(url, json=ticket)
    assert response.status_code == 304
