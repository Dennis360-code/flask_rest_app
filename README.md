# flask_rest_app

Тестовое задание тикет-система для демострации работы API.
Реализовано на Python, Flask, PostgreSQL, SQLAlchemy, uWSGI, Docker, pytest


Запуск приложения

```bash
docker-compose up --build
```
