from flask_rest_app.tickets.views import (CommentController,
                                          TicketController,
                                          TicketsController,
                                          TicketComments)


def make_routes(api):
    api.add_resource(TicketController, '/ticket/<int:pk>')
    api.add_resource(TicketComments, '/ticket/<int:pk>/comments')
    api.add_resource(TicketsController, '/tickets')
    api.add_resource(CommentController, '/comments')
