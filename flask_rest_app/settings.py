"""Application configuration.

Most configuration is set via environment variables.

For local development, use a .env file to set
environment variables.
"""
from environs import Env
import os

env = Env()
env.read_env()
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

ENV = env.str("FLASK_ENV", default="production")
DEBUG = ENV == "development"
SECRET_KEY = env.str("SECRET_KEY")
DEBUG_TB_ENABLED = DEBUG
DEBUG_TB_INTERCEPT_REDIRECTS = False
API_PREFIX = '/api/v1'

SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://" \
                          f"{env.str('POSTGRES_USER')}:" \
                          f"{env.str('POSTGRES_PASSWORD')}" \
                          f"@{env.str('POSTGRES_HOST')}/" \
                          f"{env.str('POSTGRES_DB')}"
