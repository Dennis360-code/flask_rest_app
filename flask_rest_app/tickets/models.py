import enum

from flask_rest_app.extensions import db


class StatusType(enum.Enum):
    open = 'Открыт'
    closed = 'Закрыт'
    answered = 'Отвечен'
    wip = 'Ожидает ответа'


class Ticket(db.Model):
    __tablename__ = 'ticket'

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
                              onupdate=db.func.current_timestamp())
    theme = db.Column(db.String(128), nullable=False)
    text = db.Column(db.Text, nullable=False)
    email = db.Column(db.String(128), nullable=False)
    status = db.Column(db.Enum(StatusType), nullable=False,
                       default=StatusType.open)

    def __init__(self, theme, email, text):
        self.theme = theme
        self.email = email
        self.text = text

    def __repr__(self):
        return f'<Ticket {self.id} by {self.email}>'


class Comment(db.Model):
    __tablename__ = 'comment'

    id = db.Column(db.Integer, primary_key=True)
    ticket_id = db.Column(db.Integer,
                          db.ForeignKey('ticket.id'),
                          nullable=False)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    text = db.Column(db.Text, nullable=False)
    email = db.Column(db.String(128), nullable=False)

    def __init__(self, ticket_id, text, email):
        self.ticket_id = ticket_id
        self.text = text
        self.email = email

    def __repr__(self):
        return f'<Comment {self.id} for ' \
               f'Ticket {self.ticket_id} by {self.email}>'
